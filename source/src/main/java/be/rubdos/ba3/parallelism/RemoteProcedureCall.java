package be.rubdos.ba3.parallelism;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class RemoteProcedureCall extends Thread {
    private int port;
    private ServerSocket serverSocket;
    LocalVault localVault;
    List<String> clients;

    public LocalVault getLocalVault() {
        return localVault;
    }

    private DiscoveryClient discoveryClient;
    public RemoteProcedureCall(int port)
    {
        this.port = port;
        clients = new ArrayList<>();
        localVault = new LocalVault(port);
    }

    @Override
    public void run()
    {
        System.out.println("Starting RMI on port " + port);

        InetSocketAddress serverAddress = new InetSocketAddress(port);
        try {
            serverSocket = new ServerSocket();
            serverSocket.setReuseAddress(true);
            serverSocket.bind(serverAddress);

            discoveryClient = new DiscoveryClient(this, port);
            discoveryClient.start();

            while(true)
                acceptClient();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void close()
    {
        try {
            if(!serverSocket.isClosed())
                serverSocket.close();
            discoveryClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void acceptClient() throws IOException
    {
        Socket clientSocket = serverSocket.accept(); // blocks
        (new Thread(new RemoteProdedureCallClientConnection(this, clientSocket))).start();
    }

    public void addNode(String line) {
        clients.add(line);
    }


    public byte[] get(String node, String hash) {
        String[] split = node.split(":");
        String host = split[0];
        int port = Integer.parseInt(split[1]);
        Socket socket;
        try {
            socket = new Socket(host, port);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        try {
            OutputStream os = socket.getOutputStream();
            PrintWriter pw = new PrintWriter(os);
            pw.println("get");
            pw.println(hash);
            pw.flush();

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            String line = br.readLine();
            return Hex.decodeHex(line.toCharArray());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DecoderException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public String put(VaultFile vaultFile) {
        String client = clients.get(
                ThreadLocalRandom.current().nextInt(clients.size()));
        String[] split = client.split(":");
        String host = split[0];
        int port = Integer.parseInt(split[1]);
        Socket socket;
        try {
            socket = new Socket(host, port);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        String hexFile = Hex.encodeHexString(vaultFile.getEncrypted());

        try {
            OutputStream os = socket.getOutputStream();
            PrintWriter pw = new PrintWriter(os);
            pw.println("put");
            pw.println(vaultFile.getHash());
            pw.println(hexFile);
            pw.flush();

            os.write(vaultFile.getEncrypted());

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return client;
    }
}
