package be.rubdos.ba3.parallelism;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class DiscoveryClient extends Thread {
    Socket socket;
    RemoteProcedureCall rmi;

    DiscoveryClient(RemoteProcedureCall rmi, int port) {
        this.rmi = rmi;
        try {
            socket = new Socket("discoveryserver", DiscoveryServer.port);
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            pw.println(port);
            pw.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void run()
    {
        String line;
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            while ((line = br.readLine()) != null) {
                rmi.addNode(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            if(!socket.isClosed())
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
