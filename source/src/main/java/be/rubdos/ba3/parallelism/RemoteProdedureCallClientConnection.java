package be.rubdos.ba3.parallelism;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;

class RemoteProdedureCallClientConnection implements Runnable
{
	private final RemoteProcedureCall remoteProcedureCall;
	Socket socket;
    RemoteProdedureCallClientConnection(RemoteProcedureCall remoteProcedureCall, Socket socket) {
        this.remoteProcedureCall = remoteProcedureCall;
		this.socket = socket;
    }

    @Override
    public void run() {
        try {
            InputStream is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String line;
            // First line is command
            line = br.readLine();
            if(line.equals("put")) // put command
            {
                line = br.readLine(); // line is filename/sha256sum
                String contentsHex = br.readLine();
                byte[] contents = Hex.decodeHex(contentsHex.toCharArray());
                this.remoteProcedureCall.localVault.put(line, contents);
            }
            else if(line.equals("get")) // get command
            {
                line = br.readLine(); // line is filename/sha256sum
                OutputStream os = socket.getOutputStream();
                PrintWriter pw = new PrintWriter(os);
                byte[] contents = IOUtils.toByteArray(this.remoteProcedureCall.localVault.get(line));
                pw.println(Hex.encodeHexString(contents));
                pw.flush();
                pw.close();
                os.close();
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DecoderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}