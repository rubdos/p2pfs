package be.rubdos.ba3.parallelism;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;

import jline.console.ConsoleReader;
import jline.console.completer.FileNameCompleter;
import jline.console.completer.StringsCompleter;

public class App implements Runnable
{
    RemoteVault vault;
    StringsCompleter commandCompleter;
    ConsoleReader reader;

    RemoteProcedureCall rmi;

    public App(int port)
    {
        rmi = new RemoteProcedureCall(port);
        rmi.start();

        vault = new RemoteVault(rmi);
        commandCompleter = new StringsCompleter("put", "get", "list",
                        "exit", "quit", "help");
    }
    public void close()
    {
        rmi.close();
    }
    private void printHelp()
    {
        System.out.println("Commands available:");
        System.out.println(" help:    shows this help message");
        System.out.println(" put:     Stores a local file");
        System.out.println(" get:     Gets a stored file");
        System.out.println(" list:    Lists all stored files");
        System.out.println(" exit:    Quits the program");
        System.out.println(" quit:    Quits the program");
        System.out.println(" cls:     Clears the screen");
        System.out.println(" clear:   Clears the screen");
    }
    public static void main(String[] args)
    {
        int port = 10090;
        if(args.length > 0)
            port = Integer.parseInt(args[0]);
        (new App(port)).run();
    }

    private void printList()
    {
        List<String> l = vault.list();
        assert(l != null);
        for(String filename: l)
            System.out.println(filename);
    }

    private void get()
    {
        reader.removeCompleter(commandCompleter);
        String uniqueName;
        StringsCompleter fileCompleter = new StringsCompleter(vault.list());
        try {
            // TODO: create a completer for stored files.
            reader.addCompleter(fileCompleter);
            uniqueName = reader.readLine("file?> ").trim();
            reader.removeCompleter(fileCompleter);
        } catch (IOException e1) {
            e1.printStackTrace();
            reader.removeCompleter(fileCompleter);
            reader.addCompleter(commandCompleter);
            return;
        }

        FileNameCompleter fnc = new FileNameCompleter();
        reader.addCompleter(fnc);
        String filename;
        File f;
        try {
            filename = reader.readLine("dest?> ").trim();
            Path p = FileSystems.getDefault()
                .getPath(filename).toAbsolutePath()
                .normalize();
            f = p.toFile();
        } catch (IOException e) {
            System.out.println("Did not accept file.");
            return;
        }

        try {
            byte[] contents = vault.get(uniqueName);
            FileOutputStream fos = new FileOutputStream(f);
            assert(contents != null);
            assert(fos != null);
            fos.write(contents);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("No such file.");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File unreadable.");
        }
        reader.removeCompleter(fnc);
        reader.addCompleter(commandCompleter);
    }
    private void put()
    {
        reader.removeCompleter(commandCompleter);
        FileNameCompleter fnc = new FileNameCompleter();
        reader.addCompleter(fnc);
        String filename;
        File f;
        try {
            filename = reader.readLine("file?> ").trim();
            Path p = FileSystems.getDefault()
                .getPath(filename).toAbsolutePath()
                .normalize();
            f = p.toFile();
        } catch (IOException e) {
            System.out.println("Did not accept file.");
            return;
        }

        try {
            vault.store(filename, new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("No such file.");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File unreadable.");
        }
        reader.removeCompleter(fnc);
        reader.addCompleter(commandCompleter);
    }

    @Override
    public void run() {
        try {
            reader = new ConsoleReader();

            reader.addCompleter(commandCompleter);

            reader.setPrompt("P2P> ");

            String line;
            while ((line = reader.readLine()) != null)
            {
                line = line.trim();
                if(line.equals("quit") || line.equals("exit"))
                {
                    break;
                }
                else if (line.equals("cls") || line.equals("clear"))
                {
                    reader.clearScreen();
                }
                else if(line.equals("help"))
                {
                    printHelp();
                }
                else if(line.equals("list"))
                {
                    printList();
                }
                else if(line.equals("put"))
                {
                    put();
                }
                else if(line.equals("get"))
                {
                    get();
                }
                else
                {
                    System.err.println("Command " + line + " unknown");
                }

                reader.setPrompt("P2P> ");
            }
            close();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
}
