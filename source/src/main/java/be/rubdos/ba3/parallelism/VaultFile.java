package be.rubdos.ba3.parallelism;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;

public class VaultFile {
    public String filename;
    private String hash;
    private String node;
    private SecretKey secretKey;
    private byte[] IV;
    private byte[] contents;

    public String getHash()
    {
        return hash;
    }

    public VaultFile(String uniqueName, InputStream i, RemoteProcedureCall rmi) throws IOException {
        filename = uniqueName;
        byte[] contents = IOUtils.toByteArray(i);
        hash = Hex.encodeHexString(
                (new ParallelSHA()).calculateHash(contents));

        KeyGenerator keyGen;
        try {
            keyGen = KeyGenerator.getInstance("AES");
            IV = new byte[16]; // 16 bytes initialization vector
            (new SecureRandom()).nextBytes(IV);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return;
        }
        keyGen.init(256); // for example
        secretKey = keyGen.generateKey();

        this.contents = contents;
        node = rmi.put(this);
        this.contents = null; // Let the garbage collector know we wont
                              // use this anymore.
    }

    public byte[] getEncrypted() {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING", "SunJCE");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey,
                    new IvParameterSpec(IV));
            byte[] ciphertext = cipher.doFinal(contents);
            assert(ciphertext.length % 16 == 0);
            return ciphertext;
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (BadPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return contents;
    }

    private VaultFile() {
    }

    // Serialization
    public String toString() {
        assert(filename != null);
        assert(hash != null);
        assert(secretKey != null);
        assert(node != null);
        return filename + "," + hash + ","
            + Hex.encodeHexString(secretKey.getEncoded()) + ","
            + Hex.encodeHexString(IV) + ","
            + node;
    }

    // Deserialization
    public static VaultFile fromString(String s) {
        String[] splitLine = s.split(",");
        if(splitLine.length < 5) // Need at least five fields...
            return null;
        VaultFile f = new VaultFile();
        f.filename = splitLine[0];
        f.hash = splitLine[1];
        byte[] decodedKey;
        try {
            decodedKey = Hex.decodeHex(splitLine[2].toCharArray());
            f.IV = Hex.decodeHex(splitLine[3].toCharArray());
        } catch (DecoderException e) {
            e.printStackTrace();
            return null;
        }
        f.node = splitLine[4];
        f.secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
        return f;
    }

    public String getNode() {
        return node;
    }

    public byte[] decrypt(byte[] input) {
        IvParameterSpec iv = new IvParameterSpec(IV);

        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
            byte[] plaintext = cipher.doFinal(input);
            assert((Hex.encodeHexString((
                            new ParallelSHA()).calculateHash(plaintext)))
                    .equals(hash));
            return plaintext;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (BadPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
           return null;
    }
}
