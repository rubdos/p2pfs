package be.rubdos.ba3.parallelism;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class LocalVault {
    // LocalVault asks for a port, because
    // then multiple instances can run on a single machine.
    String localVaultPath;
    LocalVault(int port) {
        Path p = FileSystems.getDefault()
            .getPath(Integer.toString(port)).toAbsolutePath()
            .normalize();
        localVaultPath = p.toFile().getAbsolutePath();

        p.toFile().mkdir();
    }

    public String getPath() {
        return localVaultPath;
    }

    InputStream get(String filename) throws FileNotFoundException {
        System.out.println("Request for " + filename);
        return new FileInputStream(localVaultPath + "/" + filename);
    }

    void put(String filename, byte[] contents) throws FileNotFoundException {
        OutputStream os = new FileOutputStream(localVaultPath + "/" + filename);
        try {
            os.write(contents);
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
