package be.rubdos.ba3.parallelism;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ParallelSHA
{
    int granularity;
    int span;
    public ParallelSHA()
    {
        this(0,1);
    }
    // 0 for granularity means whole thing at once.
    public ParallelSHA(int granularity, int span)
    {
        this.granularity = granularity;
        this.span = span;
    }

    public byte[] calculateHash(byte[] input)
    {
        SHAResult r = ForkJoinPool.commonPool().invoke(
                new SHATask(input, span));
        return r.result;
    }

    // Helper classes

    private class SHAResult
    {
        byte[] result;
        public SHAResult(byte[] r)
        {
            this.result = r;
        }
    }
    private class SHATask extends RecursiveTask<SHAResult>
    {
        private static final long serialVersionUID = 791207183442353099L;
        int begin;
        int end;
        int span;
        byte[] input;

        public SHATask(byte[] input, int span)
        {
            this(0, input.length, input, span);
        }

        public SHATask(int begin, int end, byte[] input, int span)
        {
            this.begin = begin;
            this.end = end;
            this.input = input;
            this.span = span;
        }

        @Override
        public SHAResult compute()
        {
            /* 0) Create our MessageDigest object.
             *
             * TODO: Check whether it would be useful and possible to
             *       initialize it in the task constructor or even the
             *       ParallelSHA object. */
            MessageDigest sha256;
            try {
                sha256 = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return null;
            }

            /* 1) sequential cutoff/leaf check, return AdditionResult */
            // TODO: Does this cutoff actually work?
            if(granularity == 0 || (end - begin) < granularity) {
                byte[] slice = Arrays.copyOfRange(input, begin, end);
                return new SHAResult(sha256.digest(slice));
            }

            /* 2) split work */
            int mid = end - (end-begin)/2;
            SHATask left = new SHATask(begin, mid, input, span - 1);
            SHATask right = new SHATask(mid, end, input, span - 1);

            left.fork();
            SHAResult rright = right.compute();
            SHAResult rleft = left.join();

            /* 3) Append work and combine hashes */

            byte[] result = Arrays.copyOf(rleft.result,
                    rleft.result.length + rright.result.length);
            System.arraycopy(rright.result, 0, result, rleft.result.length,
                    rright.result.length);
            if(span <= 0)
            {
                SHAResult res = new SHAResult(result);
                return res;
            }
            else
            {
                SHAResult res = new SHAResult(sha256.digest(result));
                return res;
            }
        }
    }
}
