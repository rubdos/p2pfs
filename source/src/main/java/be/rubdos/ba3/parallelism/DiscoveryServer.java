package be.rubdos.ba3.parallelism;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DiscoveryServer implements Runnable{
    public static final int port = 10000;
    public DiscoveryServer() {
        clients = new ArrayList<>();
    }

    public static void main(String[] args) {
        (new DiscoveryServer()).run();
    }

    private List<DSClient> clients;

    ServerSocket serverSocket;
    @Override
    public void run() {
        System.out.println("Starting DS on port " + port);

        InetSocketAddress serverAddress = new InetSocketAddress(port);
        try {
            serverSocket = new ServerSocket();
            serverSocket.bind(serverAddress);
            while(true)
                acceptClient();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    class DSClient {
        Socket socket;
        int port;
        String connectionString;

        public String toString()
        {
            return connectionString;
        }

        DSClient(Socket s) throws IOException {
            socket = s;

            // First read the port they listen on
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            int clientPort = Integer.parseInt(br.readLine());
            connectionString = socket.getInetAddress().getCanonicalHostName()
                + ":" + clientPort;

            System.out.println("New connection: " + connectionString);

            // First notify everyone of the new client
            // and give the new client a notion of everyone else
            OutputStream newClientOutputStream = socket.getOutputStream();
            PrintWriter newClientPrintWriter = new PrintWriter(newClientOutputStream);

            PrintWriter otherClientPrintWriter;
            DSClient other;
            for(Iterator<DSClient> it = clients.iterator(); it.hasNext();) {
                other = it.next();
                Socket otherSocket = other.socket;
                if(!otherSocket.isConnected()) {
                    System.out.println(other + " is not connected");
                    it.remove();
                    continue;
                }

                System.out.println("New client gets " + other.connectionString);
                newClientPrintWriter.println(
                        other.connectionString);

                otherClientPrintWriter = new PrintWriter(otherSocket.getOutputStream());
                otherClientPrintWriter.println(
                        connectionString);
                otherClientPrintWriter.flush();
            }
            newClientPrintWriter.flush();
        }
    }

    private void acceptClient() throws IOException
    {
        Socket clientSocket = serverSocket.accept(); // blocks
        clients.add(new DSClient(clientSocket));
    }
}
