package be.rubdos.ba3.parallelism;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class RemoteVault {

    String localIndexName = ".local_index";
    List<VaultFile> localIndex;
    RemoteProcedureCall rmi;

    public RemoteVault(RemoteProcedureCall rmi)
    {
        this.rmi = rmi;
        localIndex = new ArrayList<>();
        localIndexName = rmi.getLocalVault().getPath() + "/" + localIndexName;
        readLocalIndex();
    }

    public void store(String uniqueName, InputStream i) throws IOException {
        localIndex.add(new VaultFile(uniqueName, i, rmi));
        writeLocalIndex();
    }

    public byte[] get(String uniqueName) {
        // Linear search. Sorry.
        // TODO: make it a HashMap or so.
        for(VaultFile vf: localIndex) {
            if(vf.filename.equals(uniqueName)) {
                // Match.
                System.out.println("Found file at " + vf.getNode());
                byte[] contents;
                contents = vf.decrypt(rmi.get(vf.getNode(), vf.getHash()));
                return contents;
            }
        }
        return null;
    }

    /**
     * The local index keeps track of what files are stored, where,
     * and their checksum.
     *
     * It's stored in .local_index, as an UTF-8 csv file:
     * filename,hash
     */
    private void writeLocalIndex() {
        PrintWriter writer;
        try {
            writer = new PrintWriter(localIndexName, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
            System.err.println("Error when writing local index.");
            return;
        }
        for(VaultFile f: localIndex)
            writer.println(f);
        writer.println();
        writer.close();
    }
    private void readLocalIndex() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(localIndexName));
        } catch (FileNotFoundException e) {
            System.err.println("Cannot read local index");
            return;
        }
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                VaultFile f = VaultFile.fromString(line);
                if(f == null) continue;
                localIndex.add(f);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public List<String> list()
    {
        List<String> l = new ArrayList<>();
        for(VaultFile vf: localIndex)
            l.add(vf.filename);
        return l;
    }
}
