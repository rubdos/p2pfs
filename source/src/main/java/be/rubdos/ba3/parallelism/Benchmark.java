package be.rubdos.ba3.parallelism;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;

public class Benchmark
{
    public static int binlog( int bits ) // returns 0 for bits=0
    {
        int log = 0;
        if( ( bits & 0xffff0000 ) != 0 ) { bits >>>= 16; log = 16; }
        if( bits >= 256 ) { bits >>>= 8; log += 8; }
        if( bits >= 16  ) { bits >>>= 4; log += 4; }
        if( bits >= 4   ) { bits >>>= 2; log += 2; }
        return log + ( bits >>> 1 );
    }

    private static class Measurement
    {
        int index;
        long time;
        long deviation;
        Measurement(int i, long t, long deviation)
        {
            this.index = i;
            this.time = t;
            this.deviation= deviation;
        }
    }
    public static void main( String[] args )
    {
        System.out.println( "Generating a lot of random stuff" );
        byte[] random = new byte[1024*1024*128]; // Allocate some megabytes
        new Random().nextBytes(random);
        System.out.println( "Starting benchmark" );

        final int iterations = 256;

        // Variation of granularity
        int bestGranularity = 1;
        long bestGranularityTime = Integer.MAX_VALUE;
        long begin, end;
        ArrayList<Measurement> granularityMeasurements
            = new ArrayList<>();
        ArrayList<Long> durations = new ArrayList<>();
        for(int g = 64; g <= random.length; g *= 2)
        {
            durations.clear();
            System.out.println("G = " + g);
            ParallelSHA test = new ParallelSHA(g,1);
            long mean = 0;
            long variance = 0;
            for(int i = 0; i < iterations; ++i) {
                begin = System.nanoTime();
                test.calculateHash(random);
                end = System.nanoTime();
                long duration = end-begin;
                mean += duration;
                durations.add(duration);
            }
            mean /= iterations;
            for(Long duration: durations) {
                variance += (duration-mean)*(duration-mean);
            }
            variance /= (iterations - 1);
            granularityMeasurements.add(new Measurement(g, mean, (long)Math.sqrt(variance)));
            if(bestGranularityTime > mean)
            {
                bestGranularityTime = mean;
                bestGranularity = g;
            }
        }

        System.out.println("Best G = " + bestGranularity +
                " in " + bestGranularityTime + " ns");

        // Variation of span
        ArrayList<Measurement> spanMeasurements
            = new ArrayList<>();
        for(int s = 1; s < binlog(random.length); s++)
        {
            durations.clear();
            System.out.println("S = " + s);
            ParallelSHA test = new ParallelSHA(bestGranularity, s);
            long mean = 0;
            long variance = 0;
            for(int i = 0; i < iterations; ++i) {
                begin = System.nanoTime();
                test.calculateHash(random);
                end = System.nanoTime();
                long duration = end-begin;
                mean += duration;
                durations.add(duration);
            }
            mean /= iterations;
            for(Long duration: durations) {
                variance += (duration-mean)*(duration-mean);
            }
            variance /= (iterations - 1);
            spanMeasurements.add(new Measurement(s, mean, (long)Math.sqrt(variance)));
        }

        // Now write the csv's.
        PrintWriter writer;
        try {
            writer = new PrintWriter("granularity_benchmark.csv", "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
        writer.println("gran,time,std");
        for(Measurement m: granularityMeasurements)
        {
            writer.println(m.index + "," + ((double)m.time)/1000000000
                    + "," + ((double)m.deviation/1000000000));
        }
        writer.close();

        try {
            writer = new PrintWriter("span_benchmark.csv", "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
        writer.println("span,time,std");
        for(Measurement m: spanMeasurements)
        {
            writer.println(m.index + "," + ((double)m.time)/1000000000
                    + "," + ((double)m.deviation/1000000000));
        }
        writer.close();
    }
}

